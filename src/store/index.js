/**
 * Main data provider.
 *
 * @returns {JSX}
 */
export default function Provider (props) {
  return (
    <>
      {props.children}
    </>
  )
}
