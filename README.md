# Semiless Solid Starter

Opinionated [Solid](https://www.solidjs.com/)+[Tailwind](https://tailwindcss.com/) starter that doesn't use semicolons.

| | |
| --- | --- |
| **Bundler** | [Parcel v2](https://v2.parceljs.org/) |
| **Babel** | [StandardJS](https://standardjs.com/) |
| **Sass** | [TailwindCSS](https://tailwindcss.com/) |

This starter also includes:

- [DashDashAlias](https://gitlab.com/dashdashalias/parcel)
  Resolves dependency hell
- [Autoprefixer](https://www.npmjs.com/package/autoprefixer)
  Resolves browser prefixing

## Getting Started

You can quickly clone and discard this repository's Git history by using [`npx`](https://docs.npmjs.com/cli/v7/commands/npx) and the [`tiged`](https://github.com/tiged/tiged#readme) package:

```sh
npx tiged gitlab:enom/semiless-solid-starter
```

Install the necessary packages using NPM or Yarn:

```sh
npm install
```

### Usage

The following scripts are available to run:

```sh
npm run dev
# parcel serve src/index.html
```

```sh
npm run build
# parcel build src/index.html --no-cache --no-scope-hoist --public-url .
```

```sh
npm run lint
# eslint src/
```

```sh
npm run lint:fix
# eslint src/ --fix
```
